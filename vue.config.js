module.exports = {
  devServer: {
    proxy: 'https://back-torre.herokuapp.com/',
    https: true
    },
  transpileDependencies: [
    'vuetify'
  ]
}
