import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import './plugins/axios'
import apiaxios from 'axios'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  apiaxios,
  render: h => h(App)
}).$mount('#app')
