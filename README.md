# Front Torre Application
![Home](images/home.png?raw=true "Title")

In this application you can find a way to see the skills of a person registered in Torre. This application was built to complete the requirements for the senior software developer vacancy at Torre.

The intention of this program is to show in a different way the skills, opportunities and more data that the Torre platform delivers.

This project was built with: 
Frontside: Vue, Vuetify
Backside: Django, Django libreries (cors, json, etc)

Deployment Links in Heroku:

-  FRONT-END: https://front-torre.herokuapp.com/
-  BACK-END:  https://back-torre.herokuapp.com/

Project Links in Gitlab:

- FRONT-END: https://gitlab.com/jhonjaner11/torre_front
- BACK-END:   https://gitlab.com/jhonjaner11/torre_back



Links to Documentation

Wireframe

Use Case Diagram

Components Diagram

Progress



## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
